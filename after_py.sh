#!/bin/bash

# Generate new status.toml with last pywal color scheme
/usr/bin/wal_i3rust -c ~/.config/i3/status.toml

# Replace lockscreen with used wallpaper
lastPywal="$(ls -Art ~/.cache/wal/schemes| tail -n 1)"
wallpaper="$(jq -r '.wallpaper' ~/.cache/wal/schemes/$lastPywal)"
convert $wallpaper -resize 1920x1080  ~/wallpapers/lockscreen.png

# remove last pywal file
rm ~/.cache/wal/schemes/$lastPywal

# Reload i3
i3 reload
i3 restart